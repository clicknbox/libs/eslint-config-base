# eslint-config-base

Clicknbox JavaScript Style Guide

## Setup

Install dependencies:

```bash
npm install eslint --save-dev
npm install eslint-config-airbnb-base --save-dev
npm install eslint-plugin-import --save-dev
npm install eslint-plugin-promise --save-dev
npm install @clicknbox/eslint-config-base --save-dev
```

Create or edit `.eslintrc.json` file:

```js
{
    "extends": "@clicknbox/eslint-config-base"
}
```

## Test

To test without install fork or clone the repo and link the package on the source package:

```bash
npm link ../eslint-config-base
```
