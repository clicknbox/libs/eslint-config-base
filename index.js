module.exports = {
  plugins: ["promise"],
  extends: ["airbnb-base", "plugin:promise/recommended"],
  rules: {
    "arrow-parens": [
      1,
      "as-needed",
      { "requireForBlockBody": false }
    ],
    "no-unused-vars": [
      "error",
      {
        args: "none",
        varsIgnorePattern: "(should)|(server)|(debug)"
      }
    ],
    "prefer-destructuring": [
      "error",
      {
        array: true,
        object: true
      }
    ],
    radix: [
      "error",
      "as-needed"
    ],
    "no-param-reassign": [
      "error",
      {
        props: false
      }
    ],
    "max-len": [
      "error",
      {
        ignoreComments: true,
        ignoreStrings: true,
        ignoreTemplateLiterals: true
      }
    ],
    "no-underscore-dangle": [
      "error",
      {
        allow: ["_id"]
      }
    ],
    "comma-dangle": [
      "error",
      {
        "arrays": "only-multiline",
        "objects": "only-multiline",
        "imports": "never",
        "exports": "never",
        "functions": "only-multiline"
      }
    ],
    "no-console": ["error"],
    "eol-last": [
      "error",
      "always"
    ],
    "newline-after-var": [
      "error",
      "always"
    ],
    "padding-line-between-statements": "off",
    "newline-before-return": "off",
    "brace-style": [
      "error",
      "1tbs"
    ],
    "func-style": [
      "error",
      "declaration",
      { "allowArrowFunctions": true }
    ],
    "require-await": "warn",
    "promise/always-return": "warn",
    "promise/no-return-wrap": "error",
    "promise/param-names": "warn",
    "promise/catch-or-return": "error",
    "promise/no-native": "off",
    "promise/no-nesting": "error",
    "promise/no-promise-in-callback": "error",
    "promise/no-callback-in-promise": "error",
    "promise/avoid-new": "off",
    "promise/no-new-statics": "error",
    "promise/no-return-in-finally": "off",
    "promise/valid-params": "off",
    "promise/prefer-await-to-then": "warn",
    "promise/prefer-await-to-callbacks": "off",
    'object-shorthand': [
            'error',
            'always',
            { avoidExplicitReturnArrows: true },
        ],
    'arrow-body-style': ['error', 'as-needed'],
  }
};
